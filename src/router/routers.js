import Main from '@/view/main/main.vue'
import parentView from '@/components/parent-view'
/**
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面在切换标签后不会缓存，如果需要缓存，无需设置这个字段，而且需要设置页面组件name属性和路由配置的name一致
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */
export default [
	{
		path: '/login',
		name: 'login',
		meta: {
			title: 'Login - 登录',
			hideInMenu: true
		},
		component: () => import('@/view/login/login.vue')
	},
	{
		path: '/',
		name: '_home',
		redirect: '/home',
		component: Main,
		meta: {
			hideInMenu: true,
			notCache: true
		},
		children: [
			{
				path: '/home',
				name: 'home',
				meta: {
					hideInMenu: true,
					title: '首页',
					notCache: true,
					icon: 'md-home'
				},
				component: () => import('_m/首页/index.vue')
			}
		]
	},
	{
		path: '/401',
		name: 'error_401',
		meta: {
			hideInMenu: true
		},
		component: () => import('@/view/error-page/401.vue')
	},
	{
		path: '/500',
		name: 'error_500',
		meta: {
			hideInMenu: true
		},
		component: () => import('@/view/error-page/500.vue')
	},
	{
		path: '*',
		name: 'error_404',
		meta: {
			hideInMenu: true
		},
		component: () => import('@/view/error-page/404.vue')
	},
	//  以下是功能模块
	{
		path: '/base',
		name: 'base',
		component: Main,
		meta: {
			title: '基础平台',
			icon: 'md-home'
		},
		children: [
			{
				path: 'platform',
				name: 'platform',
				meta: {
					title: '平台管理',
					icon: 'md-home'
				},
				component: parentView,
				children:[
					{
						path: 'user',
						name: 'user',
						meta: {
							title: '用户管理',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/平台管理/用户管理.vue')
					},
					{
						path: 'office',
						name: 'office',
						meta: {
							title: '机构管理',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/平台管理/机构管理.vue')
					},
					{
						path: 'role',
						name: 'role',
						meta: {
							title: '角色管理',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/平台管理/角色管理.vue')
					},
					{
						path: 'resourceDic',
						name: 'resourceDic',
						meta: {
							title: '资源目录管理',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/平台管理/资源目录管理.vue')
					},
					{
						path: 'menu',
						name: 'menu',
						meta: {
							title: '菜单管理',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/平台管理/菜单管理.vue')
					}
				]
			},
			{
				path: 'apiDeploy',
				name: 'apiDeploy',
				meta: {
					title: 'API部署管理系统',
					icon: 'md-home'
				},
				component: parentView,
				children:[
					{
						path: 'apiServiceReg',
						name: 'apiServiceReg',
						meta: {
							title: 'API服务注册管理',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/API部署管理系统/API服务注册管理.vue')
					},
					{
						path: 'apiServiceAuth',
						name: 'apiServiceAuth',
						meta: {
							title: 'API服务认证管理',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/API部署管理系统/API服务认证管理.vue')
					},
					{
						path: 'apiServiceAcc',
						name: 'apiServiceAcc',
						meta: {
							title: 'API服务授权管理',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/API部署管理系统/API服务授权管理.vue')
					},
					{
						path: 'apiTest',
						name: 'apiTest',
						meta: {
							title: 'API测试中心',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/API部署管理系统/API测试中心.vue')
					},
					{
						path: 'apiWiki',
						name: 'apiWiki',
						meta: {
							title: 'API接口WIKI',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/API部署管理系统/API接口WIKI.vue')
					}
				]
			},
			{
				path: 'mcmas',
				name: 'mcmas',
				meta: {
					title: '接入系统集中管理',
					icon: 'md-home'
				},
				component: parentView,
				children:[
					{
						path: 'cmas',
						name: 'cmas',
						meta: {
							title: '接入系统集中管理',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/接入系统集中管理/接入系统集中管理.vue')
					},
					{
						path: 'qe',
						name: 'qe',
						meta: {
							title: '质量评测',
							icon: 'md-home'
						},
						component: () => import('_m/基础平台/接入系统集中管理/质量评测.vue')
					}
				]
			},
			{
				path: 'platformLog',
				name: 'platformLog',
				meta: {
					title: '平台日志',
					icon: 'md-home'
				},
				component: () => import('_m/基础平台/平台日志.vue')
			}
		]
	},
	
	{
		path: 'sjzyjr',
		name: 'sjzyjr',
		component: Main,
		meta: {
			title: '数据资源接入',
			icon: 'md-home'
		},
		children: [
			{
				path: 'jrqzhgl_zyjr',
				name: 'jrqzhgl_zyjr',
				meta: {
					title: '接入区综合管理',
					icon: 'md-home'
				},
				component: () => import('_m/数据资源接入/接入区综合管理.vue')
			},
			{
				path: 'lsjfw',
				name: 'lsjfw',
				meta: {
					title: '流数据服务',
					icon: 'md-home'
				},
				component: () => import('_m/数据资源接入/流数据服务.vue')
			},
			{
				path: 'rwzxzx',
				name: 'rwzxzx',
				meta: {
					title: '任务执行中心',
					icon: 'md-home'
				},
				component: parentView,
				children:[
					{
						path: 'rwzxzx_zyjr',
						name: 'rwzxzx_zyjr',
						meta: {
							title: '任务执行中心',
							icon: 'md-home'
						},
						component: () => import('_m/数据资源接入/任务执行中心/任务执行中心.vue')
					},
					{
						path: 'jrrz_zyjr_rwzxzx',
						name: 'jrrz_zyjr_rwzxzx',
						meta: {
							title: '接入日志',
							icon: 'md-home'
						},
						component: () => import('_m/数据资源接入/任务执行中心/接入日志.vue')
					}
				]
			},
			{
				path: 'sjlxgl',
				name: 'sjlxgl',
				meta: {
					title: '数据类型管理',
					icon: 'md-home'
				},
				component: () => import('_m/数据资源接入/数据类型管理.vue')
			},
			{
				path: 'sjbqgl',
				name: 'sjbqgl',
				meta: {
					title: '数据标签管理',
					icon: 'md-home'
				},
				component: () => import('_m/数据资源接入/数据标签管理.vue')
			},
			{
				path: 'sjxyfw',
				name: 'sjxyfw',
				meta: {
					title: '数据校验服务',
					icon: 'md-home'
				},
				component: () => import('_m/数据资源接入/数据校验服务.vue')
			}
		]
	},
	
	{
		path: 'yyzyjr',
		name: 'yyzyjr',
		component: Main,
		meta: {
			title: '应用数据接入',
			icon: 'md-home'
		},
		children: [
			{
				path: 'jrqzhgl_zyjr',
				name: 'jrqzhgl_zyjr',
				meta: {
					title: '接入区综合管理',
					icon: 'md-home'
				},
				component: parentView,
				children: [
					{
						path: 'sbsjjggl',
						name: 'sbsjjggl',
						meta: {
							title: '上报数据机构管理',
							icon: 'md-home'
						},
						component: () => import('_m/应用数据接入/接入区综合管理/上报数据机构管理.vue')
					},
					{
						path: 'sbsjzygl',
						name: 'sbsjzygl',
						meta: {
							title: '上报数据资源管理',
							icon: 'md-home'
						},
						component: () => import('_m/应用数据接入/接入区综合管理/上报数据资源管理.vue')
					}
				]
			},
			{
				path: 'sjtbccfw',
				name: 'sjtbccfw',
				meta: {
					title: '数据同步存储服务',
					icon: 'md-home'
				},
				component: () => import('_m/应用数据接入/数据同步存储服务.vue')
			},
			{
				path: 'sjdyfw_yysjjr',
				name: 'sjdyfw_yysjjr',
				meta: {
					title: '数据订阅服务',
					icon: 'md-home'
				},
				component: () => import('_m/应用数据接入/数据订阅服务.vue')
			},
			{
				path: 'sjapifw',
				name: 'sjapifw',
				meta: {
					title: '数据API服务',
					icon: 'md-home'
				},
				component: parentView,
				children: [
					{
						path: 'apics_sjapifw_yysjjr',
						name: 'apics_sjapifw_yysjjr',
						meta: {
							title: 'API测试',
							icon: 'md-home'
						},
						component: () => import('_m/应用数据接入/数据API服务/API测试.vue')
					},
					{
						path: 'apiwiki_sjapifw_yysjjr',
						name: 'apiwiki_sjapifw_yysjjr',
						meta: {
							title: 'API接口WIKI',
							icon: 'md-home'
						},
						component: () => import('_m/应用数据接入/数据API服务/API接口WIKI.vue')
					}
				]
			},
			{
				path: 'yhrzgl',
				name: 'yhrzgl',
				meta: {
					title: '用户认证管理',
					icon: 'md-home'
				},
				component: () => import('_m/应用数据接入/用户认证管理.vue')
			},
			{
				path: 'sjjryygl',
				name: 'sjjryygl',
				meta: {
					title: '数据接入应用管理',
					icon: 'md-home'
				},
				component: () => import('_m/应用数据接入/数据接入应用管理.vue')
			}
		]
	},
	
	{
		path: 'sjh',
		name: 'sjh',
		component: Main,
		meta: {
			title: '数据湖',
			icon: 'md-home'
		},
		children: [
			{
				path: 'sjjtfw',
				name: 'sjjtfw',
				meta: {
					title: '数据监听服务',
					icon: 'md-home'
				},
				component: () => import('_m/数据湖/数据监听服务.vue')
			},
			{
				path: 'sjyzglfw',
				name: 'sjyzglfw',
				meta: {
					title: '数据验证管理服务',
					icon: 'md-home'
				},
				component: () => import('_m/数据湖/数据验证管理服务.vue')
			},
			{
				path: 'dsjjqgl',
				name: 'dsjjqgl',
				meta: {
					title: '大数据集群管理',
					icon: 'md-home'
				},
				component: () => import('_m/数据湖/大数据集群管理.vue')
			},
			{
				path: 'fbswjjhgl',
				name: 'fbswjjhgl',
				meta: {
					title: '分布式文件交换管理',
					icon: 'md-home'
				},
				component: () => import('_m/数据湖/分布式文件交换管理.vue')
			}
		]
	},
	
	{
		path: 'sjlsx',
		name: 'sjlsx',
		component: Main,
		meta: {
			title: '数据流水线',
			icon: 'md-home'
		},
		children: [
			{
				path: 'etlglxt',
				name: 'etlglxt',
				meta: {
					title: 'ETL管理系统',
					icon: 'md-home'
				},
				component: () => import('_m/数据流水线/ETL管理系统.vue')
			},
			{
				path: 'etlkshgj',
				name: 'etlkshgj',
				meta: {
					title: 'ETL可视化工具',
					icon: 'md-home'
				},
				component: () => import('_m/数据流水线/ETL可视化工具.vue')
			},
			{
				path: 'qxgzgl',
				name: 'qxgzgl',
				meta: {
					title: '清洗规则管理',
					icon: 'md-home'
				},
				component: () => import('_m/数据流水线/清洗规则管理.vue')
			},
			{
				path: 'sjljrgl',
				name: 'sjljrgl',
				meta: {
					title: '数据流接入管理',
					icon: 'md-home'
				},
				component: () => import('_m/数据流水线/数据流接入管理.vue')
			},
			{
				path: 'rz_sjlsx',
				name: 'rz_sjlsx',
				meta: {
					title: '日志',
					icon: 'md-home'
				},
				component: () => import('_m/数据流水线/日志.vue')
			}
		]
	},
	
	{
		path: 'sjjs',
		name: 'sjjs',
		component: Main,
		meta: {
			title: '数据集市',
			icon: 'md-home'
		},
		children: [
			{
				path: 'rdmbs',
				name: 'rdmbs',
				meta: {
					title: 'RDMBS',
					icon: 'md-home'
				},
				component: () => import('_m/数据集市/RDMBS.vue')
			},
			{
				path: 'nosql',
				name: 'nosql',
				meta: {
					title: 'NOSQL',
					icon: 'md-home'
				},
				component: () => import('_m/数据集市/NOSQL.vue')
			},
			{
				path: 'sjsygl',
				name: 'sjsygl',
				meta: {
					title: '数据索引管理',
					icon: 'md-home'
				},
				component: () => import('_m/数据集市/数据索引管理.vue')
			}
		]
	},
	
	{
		path: 'sjfw',
		name: 'sjfw',
		component: Main,
		meta: {
			title: '数据服务',
			icon: 'md-home'
		},
		children: [
			{
				path: 'sjdygl_sjfw',
				name: 'sjdygl_sjfw',
				meta: {
					title: '数据订阅管理',
					icon: 'md-home'
				},
				component: () => import('_m/数据服务/数据订阅管理.vue')
			},
			{
				path: 'sjtsfw',
				name: 'sjtsfw',
				meta: {
					title: '数据推送服务',
					icon: 'md-home'
				},
				component: () => import('_m/数据服务/数据推送服务.vue')
			},
			{
				path: 'sjtbfwpz',
				name: 'sjtbfwpz',
				meta: {
					title: '数据同步服务配置',
					icon: 'md-home'
				},
				component: () => import('_m/数据服务/数据同步服务配置.vue')
			}
		]
	},
	
	{
		path: 'sjzlfw',
		name: 'sjzlfw',
		component: Main,
		meta: {
			title: '数据治理服务',
			icon: 'md-home'
		},
		children: [
			{
				path: 'ysjgl',
				name: 'ysjgl',
				meta: {
					title: '元数据管理',
					icon: 'md-home'
				},
				component: parentView,
				children: [
					{
						path: 'sjygl',
						name: 'sjygl',
						meta: {
							title: '数据元管理',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/元数据管理/数据元管理.vue')
					},
					{
						path: 'tycgl',
						name: 'tycgl',
						meta: {
							title: '同义词管理',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/元数据管理/同义词管理.vue')
					}
				]
			},
			{
				path: 'sjgxfw',
				name: 'sjgxfw',
				meta: {
					title: '数据共享服务',
					icon: 'md-home'
				},
				component: parentView,
				children: [
					{
						path: 'gxqxpz',
						name: 'gxqxpz',
						meta: {
							title: '共享权限配置',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/数据共享服务/共享权限配置.vue')
					},
					{
						path: 'mgxxpbsz',
						name: 'mgxxpbsz',
						meta: {
							title: '敏感信息屏蔽设置',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/数据共享服务/敏感信息屏蔽设置.vue')
					}
				]
			},
			{
				path: 'sjjsfw',
				name: 'sjjsfw',
				meta: {
					title: '数据检索服务',
					icon: 'md-home'
				},
				component: parentView,
				children: [
					{
						path: 'sjhs',
						name: 'sjhs',
						meta: {
							title: '数据回溯',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/数据检索服务/数据回溯.vue')
					}
				]
			},
			{
				path: 'zymlgl',
				name: 'zymlgl',
				meta: {
					title: '资源目录管理',
					icon: 'md-home'
				},
				component: parentView,
				children: [
					{
						path: 'sjdygl_zyml',
						name: 'sjdygl_zyml',
						meta: {
							title: '资源目录管理',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/资源目录管理/资源目录管理.vue')
					},
					{
						path: 'jrjggl',
						name: 'jrjggl',
						meta: {
							title: '接入机构管理',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/资源目录管理/接入机构管理.vue')
					},
					{
						path: 'jrxtgl_zyml',
						name: 'jrxtgl_zyml',
						meta: {
							title: '接入系统管理',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/资源目录管理/接入系统管理.vue')
					},
					{
						path: 'jrzy_zyml',
						name: 'jrzy_zyml',
						meta: {
							title: '接入资源管理',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/资源目录管理/接入资源管理.vue')
					},
					{
						path: 'jrxxtj_zyml',
						name: 'jrxxtj_zyml',
						meta: {
							title: '接入信息统计',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/资源目录管理/接入信息统计.vue')
					}
				]
			},
			{
				path: 'sjzlgl',
				name: 'sjzlgl',
				meta: {
					title: '数据质量管理',
					icon: 'md-home'
				},
				component: parentView,
				children: [
					{
						path: 'sjzlfkgl',
						name: 'sjzlfkgl',
						meta: {
							title: '数据质量反馈管理',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/数据质量管理/数据质量反馈管理.vue')
					},
					{
						path: 'sjzlbg',
						name: 'sjzlbg',
						meta: {
							title: '数据质量报告',
							icon: 'md-home'
						},
						component: () => import('_m/数据治理服务/数据质量管理/数据质量报告.vue')
					}
				]
			},
		]
	},
]
