## Install
```bush
// install dependencies
npm install
```
## Run
### Development
```bush
npm run dev
```
### Production(Build)
```bush
npm run build
```

## 简介
&emsp;&emsp;基于[iView admin](https://github.com/iview/iview-admin)搭建，并集成[element-ui](http://element-cn.eleme.io/#/zh-CN/component/quickstart)
## 文件结构
```shell
├── build  项目构建配置
├── public  打包所需静态资源
└── src
    ├── assets  项目静态资源
        ├── icons  自定义图标资源
        └── images  图片资源
    ├── components  业务组件
    ├── config  项目运行配置
    ├── directive  自定义指令
    ├── libs  封装工具函数
    ├── router  路由配置
    ├── store  Vuex配置
    ├── store  vuex模块
    └── view  页面文件
        ├── error-page  错误页面
        ├── login       登录页面
        ├── main        主页面框架
        └── module      功能模块页面
```

## Links
- [Vue](https://github.com/vuejs/vue)
- [Vuex](https://vuex.vuejs.org/zh/guide/)
- [iView](https://www.iviewui.com/docs/guide/start)
- [Element-UI](http://element-cn.eleme.io/#/zh-CN/component/quickstart)
- [Webpack](https://www.webpackjs.com/concepts/)
- [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects)
- [ECMAScript 6](http://es6.ruanyifeng.com/)
- [WebStorm](https://www.jetbrains.com/webstorm/)
- [Git](https://git-scm.com/)
- [Gitlab](https://gitlab.com/)

## License
apache

Copyright (c) 2017-present, KMYL
